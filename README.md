## Udacity Course "Intro to iOS App Development with Swift": Pitch Perfect App

I followed along with the Udacity course to create this app. The app records
audio and allows the user to play it back with different manipulations. This was 
my first foray into XCode, Swift, and Model-View-Control theory.

### How to use

To use a simulated version of this app on a computer, open the project in XCode, 
choose the type of device you'd like to simulate the app on, and hit the run 
button. The device simulator will pop up and you can use the app just like you
would on the actual device.